# Pyro server-client Project

### Problem statement
    
    This project was done as part of the class of Web Services and Middleware Technologies while 
    pursuing my master degree in Distributed Systems.
    
    The project represents a console-based simple implementation of a library.
    Features:
        * CRUD on books
        * CRUD on authors
        * filter books after title parts
        * filter authors after name parts 
        

### Technical details
    This project consists of 2 Pyro implementations:
        1. Pyro3 with:
            - server written in Python2.7
            - client written in Python2.7
    This version of Python was chosen for compatibility.
    
        2. Pyro4 with:
            - server written in Python3
            - client written in Python3
            - client written in Java using Pyrolite 4.3
            
    
    Used dependencies for both projects:
        pyro4 - pip3 install pyro4 (for Pyro4)
        pyro - pip/pip3 install pyro (for Pyro3)
        sqlalchemy - pip/pip3 install sqlalchemy
        mysql-connector-python - pip/pip3 install mysql-connector-python
        
     Maven dependency used: 
     <dependency>
         <groupId>net.razorvine</groupId>
         <artifactId>pyrolite</artifactId>
         <version>4.30</version>
     </dependency>
     
     As ORM was used sqlalchemy and as DB - mysql.
     Prerequisite: a db called bilbio
            

### About Pyro 

    Pyro comes from PYthon Remote Objects. It is a library that enables one to build applications in which objects
    can talk to each other over the network. It uses RPC(Remote Procedure Call) as a method of communication.
    Also objects are represented as Proxies.
    
### Documentation and useful links
    
    1. Pyro3 code also with examples - https://github.com/irmen/Pyro3
    2. Pyro4 code also with examples - https://github.com/irmen/Pyro4
    3. Pyrolite Java (and C#) code also with examples - https://github.com/irmen/Pyrolite
    4. Easy to follow tutorial and documentation for Pyro4 - https://pyro4.readthedocs.io/en/stable/tutorials.html
    5. SqlAlchemy tutorial - https://auth0.com/blog/sqlalchemy-orm-tutorial-for-python-developers/
    6. How to use mysql-connector - https://mysql.wisborg.dk/2019/03/03/using-sqlalchemy-with-mysql-8/
    
