from sqlalchemy import create_engine, Column, String, Integer, Table, ForeignKey
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import Pyro4

Base = declarative_base()

movies_actors_association = Table(
    'books_authors', Base.metadata,
    Column('book_id', Integer, ForeignKey('books.id')),
    Column('author_id', Integer, ForeignKey('authors.id'))
)

@Pyro4.expose
class Book(Base):

    __tablename__ = 'books'
    _id = Column('id',Integer, primary_key=True)
    _title = Column('title', String(100))
    _year = Column('year' ,Integer)
    _authors = relationship("Author", secondary=movies_actors_association)

    def __init__(self, title, year):
        self.title = title
        self.year = year

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def year(self):
        return self._year

    @year.setter
    def year(self, value):
        self._year = value

    @property
    def authors(self):
        return self._authors

    @authors.setter
    def authors(self, value):
        self._authors = value

    @property
    def id(self):
        return self._id

    def update(self, title=None, year=None):
        if title is not None:
            self.title = title
        if year is not None:
            self.year = year

@Pyro4.expose
class Author(Base):

    __tablename__ = 'authors'
    _id = Column('id', Integer, primary_key=True)
    _name = Column('name', String(100))
    _age = Column('age' ,Integer)

    def __init__(self, name, age):
        self.name = name
        self.age = age

    @property
    def id(self):
        return self._id

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, value):
        self._age = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    def update(self, name=None, age=None):
        if name is not None:
            self.name = name

        if age is not None:
            self.age = age


def init_db():
    uri = 'mysql+mysqlconnector://root:root@localhost:3306/biblio'
    engine = create_engine(uri)
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False,
                                             bind=engine))
    Base.query = db_session.query_property()
    Base.metadata.create_all(bind=engine)
    return db_session