import orm
from service import Service
import Pyro4

if __name__ == '__main__':
    db_session = orm.init_db()
    service = Service(db_session)

    with Pyro4.Daemon(port=7543) as daemon:
        service_uri = daemon.register(service, 'example.service.biblio')
        print("Pyro4 server is available....")
        print('URI: ' + str(service_uri))
        daemon.requestLoop()

