from orm import Book
from orm import Author
import Pyro4

@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class Service(object):

    def __init__(self, session):
        self.session = session
        self._registered_objects = []

    def add_book(self, title, year, authors=None):
        book = Book(title, year)
        if  authors is not None:
            book.authors = authors

        self.session.add(book)
        self.session.commit()

    def get_books_filtered_by_title(self, title):
        self._unregister_objects()
        filtered_books = self.session.query(Book).filter(Book._title.contains(title)).all()
        self._register_objects(filtered_books)
        return filtered_books

    def update_book(self, book_id, title=None, year=None):
        book = self.session.query(Book).get(book_id)
        book.update(title, year)
        self.session.commit()

    def delete_book(self, book_id):
        book = self.session.query(Book).get(book_id)
        self.session.delete(book)
        self.session.commit()

    def add_authors_to_book(self, book_id, authors):
        book = self.session.query(Book).get(book_id)
        book.authors = authors

    def get_all_books(self):
        self._unregister_objects()
        books = self.session.query(Book).all()
        self._register_objects(books)
        return books

    def add_author(self, name, age):
        author = Author(name, age)
        self.session.add(author)
        self.session.commit()

    def update_author(self, author_id, name=None, age=None):
        author = self.session.query(Author).get(author_id)
        author.update(name, age)
        self.session.commit()

    def delete_author(self, author_id):
        author = self.session.query(Author).get(author_id)
        self.session.delete(author)
        self.session.commit()

    def get_all_authors(self):
        self._unregister_objects()
        authors = self.session.query(Author).all()
        self._register_objects(authors)
        return authors

    def _register_objects(self, list):
        for obj in list:
            self._pyroDaemon.register(obj)
            self._registered_objects.append(obj)

    def _unregister_objects(self):
        for object in self._registered_objects:
            self._pyroDaemon.unregister(object)
        self._registered_objects.clear()

    def get_authors_filtered_by_name(self, name):
        self._unregister_objects()
        filtered_authors = self.session.query(Author).filter(Author._name.contains(name)).all()
        self._register_objects(filtered_authors)
        return filtered_authors

    def add_relation(self, book_id, author_id):
        book = self.session.query(Book).get(book_id)
        author = self.session.query(Author).get(author_id)
        book.authors.append(author)
        self.session.commit()