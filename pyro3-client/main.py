from bookmenu import BookMenu
from authormenu import AuthorMenu
from relationmenu import RelationMenu
import Pyro.core

def print_main_menu():
    print
    print('1. Book menu')
    print('2. Authors menu')
    print('3. Add relation')
    print('e  -> to exit')
    print

if __name__ == '__main__':
    service = Pyro.core.getProxyForURI('PYRO://127.0.1.1:7543/7f000101293b2503118308e551b3d2db8f')

    stop = False
    bookMenu = BookMenu(service)
    authorMenu = AuthorMenu(service)
    relationMenu = RelationMenu(service)
    while not stop:
        print_main_menu()
        ch = raw_input('Your input: ')


        if ch == '1':
            bookMenu.start()
        elif ch == '2':
            authorMenu.start()
        elif ch == '3':
            relationMenu.start()
        elif ch == 'e':
            stop = True
        else:
            print('Invalid input...')