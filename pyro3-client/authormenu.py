import Pyro.core

class AuthorMenu:

    def __init__(self, service):
        self.service = service

    def start(self):
        stop = False
        while not stop:
            self.print_menu()

            ch = raw_input()
            if ch == '0':
                self.print_authors(self.service.get_all_authors())
            elif ch == '1':
                self.add_author()
            elif ch == '2':
                self.update_author()
            elif ch == '3':
                self.delete_author()
            elif ch == '4':
                self.filter_author()
            elif ch == 'b':
                stop = True
            else:
                print('Invalid input...')

    def update_author(self):
        self.print_authors(self.service.get_all_authors())
        id = raw_input('Give author id: ')
        name = raw_input('Give new name: ')
        age = raw_input('Give new age: ')

        if not name.isalpha():
            name = None

        if age.isdigit():
            age = int(age)
        else:
            age = None

        self.service.update_author(id, name, age)
        self.print_authors(self.service.get_all_authors())

    def delete_author(self):
        self.print_authors(self.service.get_all_authors())
        id = raw_input('Give author id: ')
        self.service.delete_author(id)
        self.print_authors(self.service.get_all_authors())

    def filter_author(self):
        filter_input = raw_input('Filter by name: ')
        authors = self.service.get_authors_filtered_by_name(filter_input)
        self.print_authors(authors)

    def add_author(self):
        name = raw_input('Give Author name: ')
        age = int(raw_input('Give Author age: '))

        self.service.add_author(name, age)
        authors = self.service.get_all_authors()
        self.print_authors(authors)

    def print_authors(self, authors):
        print
        print('ID|NAME|AGE')
        print('---------')

        for author in authors:
            print(str(author.id) + " " + author.name + " " + str(author.age))
        print

    def print_menu(self):
        print
        print('0. Get All Authors')
        print('1. Add Author')
        print('2. Update Author')
        print('3. Delete Author')
        print('4. Filter Author')
        print('b->To get back to previous menu ')
