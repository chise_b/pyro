
class BookMenu:

    def __init__(self, service):
        self.service = service

    def start(self):
        stop = False
        while not stop:
            self.print_menu()

            ch = raw_input()
            if ch == '0':
                self.print_books(self.service.get_all_books())
            elif ch == '1':
                self.add_book()
            elif ch == '2':
                self.update_book()
            elif ch == '3':
                self.delete_book()
            elif ch == '4':
                self.filter_book()
            elif ch == 'b':
                stop = True
            else:
                print('Invalid input...')

    def add_book(self):
        title = raw_input('Give Book Title: ')
        year = int(raw_input('Give Book year: '))

        self.service.add_book(title, year)
        authors = self.service.get_all_books()
        self.print_books(authors)

    def update_book(self):
        self.print_books(self.service.get_all_books())
        id = raw_input('Give Book Id: ')
        title = raw_input('Give new Title: ')
        year = raw_input('Give new age: ')

        if not title.isalpha():
            title = None

        if year.isdigit():
            year = int(year)
        else:
            year = None

        self.service.update_book(id, title, year)
        self.print_books(self.service.get_all_books())

    def delete_book(self):
        self.print_books(self.service.get_all_books())
        id = raw_input('Give Book Id: ')
        self.service.delete_book(id)
        self.print_books(self.service.get_all_books())

    def filter_book(self):
        filter_input = raw_input('Filter by title: ')
        books = self.service.get_books_filtered_by_title(filter_input)
        self.print_books(books)

    def print_books(self, books):
        print
        print('ID|TITLE|YEAR')
        print('---------------')

        for book in books:
            print(str(book.id) + " " + book.title + " " + str(book.year))
        print

    def print_menu(self):
        print
        print('0. Get All Books')
        print('1. Add Book')
        print('2. Update Book')
        print('3. Delete Book')
        print('4. Filter Book')
        print('b->To get back to previous menu ')
        print