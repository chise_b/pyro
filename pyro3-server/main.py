import orm
from service import Service
import Pyro.core

if __name__ == '__main__':
    db_session = orm.init_db()
    service = Service(db_session)
    Pyro.core.initServer()
    daemon = Pyro.core.Daemon(port=7543)
    service_uri = daemon.connect(service, "ls")
    print("Pyro3 server is available....")
    print('URI: ' + str(service_uri))
    daemon.requestLoop()

