import Pyro.core


class Author(Pyro.core.ObjBase):

    def __init__(self, id, name, age):
        Pyro.core.ObjBase.__init__(self)
        self.name = name
        self.age = age
        self.id = id

class Book(Pyro.core.ObjBase):

    def __init__(self, id, title, year):
        Pyro.core.ObjBase.__init__(self)
        self.title = title
        self.year = year
        self.id = id