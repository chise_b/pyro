from orm import Book
from orm import Author
import Pyro.core
import views

class Service(Pyro.core.ObjBase):

    def __init__(self, session):
        Pyro.core.ObjBase.__init__(self)
        self.session = session
        self._registered_objects = []

    def add_book(self, title, year, authors=None):
        book = Book(title, year)
        if authors is not None:
            book.authors = authors

        self.session.add(book)
        self.session.commit()

    def get_books_filtered_by_title(self, title):
        filtered_books = self.session.query(Book).filter(Book.title.contains(title)).all()
        return self.create_books_view(filtered_books)

    def update_book(self, book_id, title=None, year=None):
        book = self.session.query(Book).get(book_id)
        book.update(title, year)
        self.session.commit()

    def delete_book(self, book_id):
        book = self.session.query(Book).get(book_id)
        self.session.delete(book)
        self.session.commit()

    def add_authors_to_book(self, book_id, authors):
        book = self.session.query(Book).get(book_id)
        book.authors = authors

    def get_all_books(self):
        books = self.session.query(Book).all()
        return self.create_books_view(books)

    def create_books_view(self, books):
        proxy_books = []
        for book in books:
            bBook = views.Book(book.id, book.title, book.year)
            self.getDaemon().connect(bBook)
            proxy_books.append(bBook.getAttrProxy())

        return proxy_books

    def add_author(self, name, age):
        author = Author(name, age)
        self.session.add(author)
        self.session.commit()

    def update_author(self, author_id, name=None, age=None):
        author = self.session.query(Author).get(author_id)
        author.update(name, age)
        self.session.commit()

    def delete_author(self, author_id):
        author = self.session.query(Author).get(author_id)
        self.session.delete(author)
        self.session.commit()

    def get_all_authors(self):
        authors = self.session.query(Author).all()

        return self.create_authors_view(authors)

    def get_authors_filtered_by_name(self, name):
        filtered_authors = self.session.query(Author).filter(Author.name.contains(name)).all()
        return self.create_authors_view(filtered_authors)

    def create_authors_view(self, authors):
        proxy_authors = []
        for author in authors:
            vAuthor = views.Author(author.id, author.name, author.age)
            self.getDaemon().connect(vAuthor)
            proxy_authors.append(vAuthor.getAttrProxy())

        return proxy_authors

    def add_relation(self, book_id, author_id):
        book = self.session.query(Book).get(book_id)
        author = self.session.query(Author).get(author_id)
        book.authors.append(author)
        self.session.commit()