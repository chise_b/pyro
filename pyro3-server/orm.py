from sqlalchemy import create_engine, Column, String, Integer, Table, ForeignKey
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import Pyro.core

Base = declarative_base()

movies_actors_association = Table(
    'books_authors', Base.metadata,
    Column('book_id', Integer, ForeignKey('books.id')),
    Column('author_id', Integer, ForeignKey('authors.id'))
)

class Book(Pyro.core.ObjBase, Base):

    __tablename__ = 'books'
    id = Column('id',Integer, primary_key=True)
    title = Column('title', String(100))
    year = Column('year' ,Integer)
    authors = relationship("Author", secondary=movies_actors_association)

    def __init__(self, title, year):
        Pyro.core.ObjBase.__init__(self)
        self.title = title
        self.year = year

    def update(self, title=None, year=None):
        if title is not None:
            self.title = title
        if year is not None:
            self.year = year


class Author(Pyro.core.ObjBase, Base):

    __tablename__ = 'authors'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(100))
    age = Column('age' ,Integer)

    def __init__(self, name, age):
        Pyro.core.ObjBase.__init__(self)
        self.name = name
        self.age = age

    def update(self, name=None, age=None):
        if name is not None:
            self.name = name

        if age is not None:
            self.age = age


def init_db():
    uri = 'mysql+mysqlconnector://root:root@localhost:3306/biblio'
    engine = create_engine(uri)
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False,
                                             bind=engine))
    Base.query = db_session.query_property()
    Base.metadata.create_all(bind=engine)
    return db_session