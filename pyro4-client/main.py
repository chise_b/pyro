from bookmenu import BookMenu
from authormenu import AuthorMenu
from relationmenu import RelationMenu
import Pyro4

def print_main_menu():
    print()
    print('1. Book menu')
    print('2. Authors menu')
    print('3. Add relation')
    print('e  -> to exit')
    print()

if __name__ == '__main__':
    service = Pyro4.Proxy('PYRO:example.service.biblio@localhost:7543')

    stop = False
    bookMenu = BookMenu()
    authorMenu = AuthorMenu()
    relationMenu = RelationMenu()
    while not stop:
        print_main_menu()
        ch = input('Your input: ')


        if ch == '1':
            bookMenu.start()
        elif ch == '2':
            authorMenu.start()
        elif ch == '3':
            relationMenu.start()
        elif ch == 'e':
            stop = True
        else:
            print('Invalid input...')