import Pyro4

class RelationMenu:

    def __init__(self):
        self.service = Pyro4.Proxy('PYRO:example.service.biblio@localhost:7543')

    def start(self):
        stop = False
        while not stop:
            print('a -> add relation')
            print('b -> go back to previous menu')
            print()

            ch = input()
            if ch == 'b':
                stop = True
            else:
                self.print_books(self.service.get_all_books())
                book_id = input('Give Book Id: ')
                self.print_authors(self.service.get_all_authors())
                author_id = input('Give Author Id')
                self.service.add_relation(book_id, author_id)

    def print_books(self, books):
        print()
        print('ID|TITLE|YEAR')
        print('---------------')

        for book in books:
            print(str(book.id) + " " + book.title + " " + str(book.year))
        print()

    def print_authors(self, authors):
        print()
        print('ID|NAME|AGE')
        print('---------')

        for author in authors:
            print(str(author.id) + " " + author.name + " " + str(author.age))
        print()