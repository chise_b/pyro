package com.example;

import com.example.menu.AuthorMenu;
import com.example.menu.BookMenu;
import com.example.menu.RelationMenu;
import net.razorvine.pyro.PyroProxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Main {

    private static PyroProxy proxy = null;
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private static void printMainMenu() {
        System.out.println();
        System.out.println("1. Book Menu");
        System.out.println("2. Authors Menu");
        System.out.println("3. Add Relation");
        System.out.println("e -> to exit");
        System.out.println();
    }

    public static void main(String[] args) {

        try {
            boolean stop = false;
            BookMenu bookMenu = new BookMenu();
            AuthorMenu authorMenu = new AuthorMenu();
            RelationMenu relationMenu = new RelationMenu();

            while (!stop) {
                printMainMenu();

                String ch = reader.readLine();
                if (ch.equals("1")) {
                    bookMenu.start();
                } else if (ch.equals("2")) {
                    authorMenu.start();
                } else if (ch.equals("3")) {
                    relationMenu.start();
                }
                else if (ch.equals("e")) {
                    stop = true;
                }
                else {
                    System.out.println("Invalid input...");
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
