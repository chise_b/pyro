package com.example;

import net.razorvine.pyro.PyroProxy;

import java.io.IOException;

public class Proxy {

    private static final String server = "example.service.biblio";
    private static final String host = "localhost";
    private static final int port = 7543;

    private static PyroProxy proxy = null;

    public static PyroProxy getInstance() throws IOException {

        if (proxy == null) {
            proxy = new PyroProxy(host, port, server);
        }
        return proxy;
    }
}
