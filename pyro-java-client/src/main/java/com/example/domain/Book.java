package com.example.domain;

import java.util.List;

public class Book {

    private Integer id;
    private String title;
    private Integer year;
    private List<Author> authors;

    public Book(Integer id, String title, Integer year, List<Author> authors) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.authors = authors;
    }

    public Book(Integer id, String title, Integer year) {
        this.id = id;
        this.title = title;
        this.year = year;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }
}
