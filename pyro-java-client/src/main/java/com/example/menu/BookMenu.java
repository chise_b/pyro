package com.example.menu;

import com.example.Proxy;
import net.razorvine.pyro.PyroProxy;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class BookMenu {

    private PyroProxy proxy = null;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public BookMenu() {
        try {
            proxy = Proxy.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printMenu() {
        System.out.println();
        System.out.println("0. Get All Books");
        System.out.println("1. Add Book");
        System.out.println("2. Update Book");
        System.out.println("3. Delete Book");
        System.out.println("4. Filter Book");
        System.out.println("b->To get back to previous menu");
    }

    public void start() throws IOException {
        boolean stop = false;

        while (!stop) {
            printMenu();

            String ch = reader.readLine();
            if (ch.equals("0")) {
                List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_books");
                printBooks(result);
            } else if (ch.equals("1")) {
                addBook();
            } else if (ch.equals("2")) {
                updateBook();
            } else if (ch.equals("3")) {
                deleteBook();
            } else if (ch.equals("4")) {
                filterBook();
            } else if (ch.equals("b")) {
                stop = true;
            } else {
                System.out.println("Invalid input...");
            }

        }
    }

    private void filterBook() throws IOException {
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_books");
        printBooks(result);

        System.out.println("Filter by title: ");
        String filterInput = reader.readLine();

        result = (List<PyroProxy>) proxy.call("get_books_filtered_by_title", filterInput);
        printBooks(result);
    }

    private void deleteBook() throws IOException {
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_books");
        printBooks(result);

        System.out.println("Give Book Id: ");
        String id = reader.readLine();

        proxy.call("delete_book", id);
        result = (List<PyroProxy>) proxy.call("get_all_books");
        printBooks(result);
    }


    private void updateBook() throws IOException {
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_books");
        printBooks(result);

        System.out.println("Give Book Id: ");
        Integer id = Integer.parseInt(reader.readLine());
        System.out.println("Give Book Title: ");
        String title = reader.readLine();
        System.out.println("Give Book Year: ");
        String year = reader.readLine();
        Integer intYear = null;

        if (!StringUtils.isAlpha(title) || title.length() == 0) {
            title = null;
        }

        if (StringUtils.isNumeric(year)) {
            intYear = Integer.parseInt(year);
        }

        proxy.call("update_book", id, title, intYear);
        result = (List<PyroProxy>) proxy.call("get_all_books");
        printBooks(result);
    }

    private void addBook() throws IOException {
        System.out.println("Give Book Title: ");
        String title = reader.readLine();
        System.out.println("Give Book Year: ");
        Integer year = Integer.parseInt(reader.readLine());

        proxy.call("add_book", title, year);
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_books");
        printBooks(result);
    }

    private void printBooks(List<PyroProxy> proxyBooks) throws IOException {
        System.out.println();
        System.out.println("ID|TITLE|YEAR");
        System.out.println("----------------");
        for (PyroProxy pyroProxy : proxyBooks) {
            Object objectId = pyroProxy.getattr("id");
            int id = (Integer) objectId;
            Object objectYear = pyroProxy.getattr("year");
            int year = (Integer) objectYear;
            Object objectTitle = pyroProxy.getattr("title");
            String title = (String) objectTitle;
            System.out.println("" + id + " " + title + " " + year);
        }
    }
}
