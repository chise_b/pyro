package com.example.menu;

import com.example.Proxy;
import net.razorvine.pyro.PyroProxy;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class AuthorMenu {

    private PyroProxy proxy = null;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public AuthorMenu() {
        try {
            proxy = Proxy.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws IOException {
        boolean stop = false;

        while (!stop) {
            printMenu();

            String ch = reader.readLine();
            if (ch.equals("0")) {
                List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_authors");
                printAuthors(result);
            } else if (ch.equals("1")) {
                addAuthor();
            } else if (ch.equals("2")) {
                updateAuthor();
            } else if (ch.equals("3")) {
                deleteAuthor();
            } else if (ch.equals("4")) {
                filterAuthor();
            } else if (ch.equals("b")) {
                stop = true;
            } else {
                System.out.println("Invalid input...");
            }

        }
    }

    private void filterAuthor() throws IOException {
        System.out.println("Filter by name: ");
        String filterInput = reader.readLine();

        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_authors_filtered_by_name", filterInput);
        printAuthors(result);
    }

    private void deleteAuthor() throws IOException {
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_authors");
        printAuthors(result);

        System.out.println("Give Author Id: ");
        String id = reader.readLine();

        proxy.call("delete_author", id);

        result = (List<PyroProxy>) proxy.call("get_all_authors");
        printAuthors(result);

    }

    private void updateAuthor() throws IOException {
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_authors");
        printAuthors(result);

        System.out.println("Give Author ID: ");
        String id = reader.readLine();
        System.out.println("Give New Name: ");
        String name = reader.readLine();
        System.out.println("Give New Age");
        String age = reader.readLine();
        Integer intAge = null;

        if (!StringUtils.isAlpha(name) || name.length() == 0) {
            name = null;
        }

        if (StringUtils.isNumeric(age)) {
            intAge = Integer.parseInt(age);
        }

        proxy.call("update_author", id, name, intAge);
        result = (List<PyroProxy>) proxy.call("get_all_authors");
        printAuthors(result);
    }

    private void addAuthor() throws IOException {
        System.out.println("Give Author Name: ");
        String name = reader.readLine();
        System.out.println("Give Author Age: ");
        Integer age = Integer.parseInt(reader.readLine());

        proxy.call("add_author", name, age);
        List<PyroProxy> result = (List<PyroProxy>) proxy.call("get_all_authors");
        printAuthors(result);
    }

    private void printAuthors(List<PyroProxy> proxyAuthors) throws IOException {
        System.out.println();
        System.out.println("ID|NAME|AGE");
        System.out.println("----------------");
        for (PyroProxy pyroProxy : proxyAuthors) {
            Object objectId = pyroProxy.getattr("id");
            int id = (Integer) objectId;
            Object objectName = pyroProxy.getattr("name");
            String name = (String) objectName;
            Object objectAge = pyroProxy.getattr("age");
            Integer age = (Integer) objectAge;
            System.out.println("" + id + " " + name + " " + age);
        }
    }

    private void printMenu() {
        System.out.println();
        System.out.println("0. Get All Authors");
        System.out.println("1. Add Author");
        System.out.println("2. Update Author");
        System.out.println("3. Delete Author");
        System.out.println("4. Filter Author");
        System.out.println("b->To get back to previous menu");
    }
}
