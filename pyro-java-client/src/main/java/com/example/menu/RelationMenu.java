package com.example.menu;

import com.example.Proxy;
import net.razorvine.pyro.PyroProxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class RelationMenu {

    private PyroProxy proxy = null;
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public RelationMenu() {
        try {
            proxy = Proxy.getInstance();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() throws IOException {
        boolean stop = false;

        while (!stop) {

            System.out.println("a -> add relation");
            System.out.println("b -> go back to previous menu");

            String ch = reader.readLine();
            if (ch.equals("b")){
                stop = true;
            }
            else{
                List<PyroProxy> bookResult = (List<PyroProxy>) proxy.call("get_all_books");
                printBooks(bookResult);
                System.out.println("Give Book Id");
                String bookId = reader.readLine();

                List<PyroProxy> authorResult = (List<PyroProxy>) proxy.call("get_all_authors");
                printAuthors(authorResult);

                System.out.println("Give Author Id");
                String authorId = reader.readLine();

                proxy.call("add_relation", bookId, authorId);
            }

        }
    }

    private void printAuthors(List<PyroProxy> proxyAuthors) throws IOException {
        System.out.println();
        System.out.println("ID|NAME|AGE");
        System.out.println("----------------");
        for (PyroProxy pyroProxy : proxyAuthors) {
            Object objectId = pyroProxy.getattr("id");
            int id = (Integer) objectId;
            Object objectName = pyroProxy.getattr("name");
            String name = (String) objectName;
            Object objectAge = pyroProxy.getattr("age");
            Integer age = (Integer) objectAge;
            System.out.println("" + id + " " + name + " " + age);
        }
    }

    private void printBooks(List<PyroProxy> proxyBooks) throws IOException {
        System.out.println();
        System.out.println("ID|TITLE|YEAR");
        System.out.println("----------------");
        for (PyroProxy pyroProxy : proxyBooks) {
            Object objectId = pyroProxy.getattr("id");
            int id = (Integer) objectId;
            Object objectYear = pyroProxy.getattr("year");
            int year = (Integer) objectYear;
            Object objectTitle = pyroProxy.getattr("title");
            String title = (String) objectTitle;
            System.out.println("" + id + " " + title + " " + year);
        }
    }

}
